import 'dart:developer';
import 'dart:typed_data';

import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BlueThermalPrinterPage extends StatefulWidget {
  const BlueThermalPrinterPage({ Key? key }) : super(key: key);

  @override
  State<BlueThermalPrinterPage> createState() => _BlueThermalPrinterPageState();
}

class _BlueThermalPrinterPageState extends State<BlueThermalPrinterPage> {

  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;
  List<BluetoothDevice> _devices = [];
  BluetoothDevice? _connectedDevice;

  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 15)).then((_) async {
      try {

        List<BluetoothDevice> r = [];

        for (int i=0; i<1; ++i) {
          await Future.delayed(const Duration(seconds: 1));
          r = await bluetooth.getBondedDevices();
          log(r.map((e) => _printDevice(e)).toString(), name: 'Device');
        }

        setState(() {
          _devices = r;
        });

        
      } catch (e) {
        log(e.toString(), name: 'Erreur');
      }

    });
  }

  String _printDevice(BluetoothDevice d) {
    return 'Device(name: ${d.name}, type: ${d.type}, address: ${d.address})';
  }

  void _connectDevice(BluetoothDevice device) async {
    try {
      await bluetooth.connect(device);
      setState(() { _connectedDevice = device; });
    } catch (e) {
      log(e.toString(), name: 'Erreur lors de la connexion');
    }
  }

  void _print() async {

    final ByteData data = await rootBundle.load('assets/lastest.jpg');
    final Uint8List buffer = data.buffer.asUint8List();

    bluetooth.printImageBytes(buffer);

    bluetooth.printNewLine();
    bluetooth.printNewLine();

    bluetooth.printCustom('Référence transaction: 123456789', 1, 0, charset: "utf-8");

    bluetooth.printCustom('Montant: 4820 FCFA', 1, 0, charset: "utf-8");
    bluetooth.printCustom('Service: Taxe de résidence', 1, 0, charset: "utf-8");
    bluetooth.printCustom('Mode paiement: MOBICASH', 1, 0, charset: "utf-8");
    bluetooth.printCustom('Status: En attente', 1, 0, charset: "utf-8");
    bluetooth.printCustom('Date: 06/08/2020 à 15:28', 1, 0, charset: "utf-8");

    bluetooth.printNewLine();
    bluetooth.printNewLine();

    bluetooth.printQRcode('{"id":1}', 200, 200, 1);

    bluetooth.printNewLine();
    bluetooth.printNewLine();
    bluetooth.printNewLine();

    bluetooth.paperCut();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: const Text('Blue Thermal Printer')
      ),
      body: ListView.builder(
        itemCount: _devices.length,
        itemBuilder: (_, index) {
          final device = _devices[index];
          return ListTile(
            onTap: () => _connectDevice(device),
            leading: device.connected
              ? const Icon(Icons.bluetooth_connected)
              : const Icon(Icons.bluetooth_disabled),
            title: Text(device.name.toString()),
            subtitle: Text(
              '${device.type} | ${device.address.toString()}' 
            ),
            trailing: device == _connectedDevice
              ? IconButton(
                  onPressed: _print,
                  icon: const Icon(Icons.print)
                )
              : null,
          );
        }
      ),
    );
  }
}
