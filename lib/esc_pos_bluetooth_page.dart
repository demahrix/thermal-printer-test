import 'dart:typed_data';

import 'package:flutter/material.dart' hide Image;
import 'package:esc_pos_bluetooth/esc_pos_bluetooth.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter/services.dart';
import 'package:image/image.dart';

class EscPosBluetoothPage extends StatefulWidget {
  const EscPosBluetoothPage({ Key? key }) : super(key: key);

  @override
  State<EscPosBluetoothPage> createState() => _EscPosBluetoothPageState();
}

class _EscPosBluetoothPageState extends State<EscPosBluetoothPage> {

  PrinterBluetoothManager printerManager = PrinterBluetoothManager();
  List<PrinterBluetooth> _devices = [];
  PrinterBluetooth? _connectedDevice;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      printerManager.startScan(const Duration(seconds: 10));
      printerManager.scanResults.listen((devices) async {
        setState(() { _devices = devices; });
      });
    });

  }

  void _connectDevice(PrinterBluetooth device) {
    printerManager.selectPrinter(_devices.first);
    setState(() { _connectedDevice = device; });
  }

  void _print() async {

    final profile = await CapabilityProfile.load();

    final Generator ticket = Generator(PaperSize.mm80, profile);

    List<int> bytes = [];

    // bytes += ticket.text('AZERKA', styles: const PosStyles(
    //   align: PosAlign.center,
    //   bold: true,
    //   height: PosTextSize.size3,
    //   width: PosTextSize.size3,
    // ));

    final ByteData data = await rootBundle.load('assets/lastest_1.jpg');
    final Uint8List buffer = data.buffer.asUint8List();
    final Image image = decodeImage(buffer)!;

    bytes += ticket.image(image);

    bytes += ticket.text(
      'Référence transaction: 123456789',
      styles: const PosStyles(
        codeTable: 'CP1252'
      )
    );

    bytes += ticket.text(
      'Montant: 4820 FCFA',
      styles: const PosStyles(
        codeTable: 'CP1252',
        align: PosAlign.left
      )
    );

    bytes += ticket.text(
      'Service: Taxe de résidence'
    );

    bytes += ticket.text(
      'Mode paiement: MOBICASH',
      styles: const PosStyles(
        codeTable: 'CP1252'
      )
    );

    bytes += ticket.text(
      'Status: En attente',
      styles: const PosStyles(
        codeTable: 'CP1252'
      )
    );

    bytes += ticket.text(
      'Date: 06/08/2020 à 15:28',
      styles: const PosStyles(
        codeTable: 'CP1252'
      )
    );

    bytes += ticket.feed(2);

    bytes += ticket.qrcode('{"id":1}');

    // bytes += ticket.feed(10);
    bytes += ticket.cut();

    printerManager.printTicket(bytes);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Esc Pos Bluetooth Page'),
      ),
      body: ListView.builder(
        itemCount: _devices.length,
        itemBuilder: (_, index) {
          final device = _devices[index];
          return ListTile(
            onTap: () => _connectDevice(device),
            title: Text(device.name ?? 'N/A'),
            subtitle: Text(
              '${device.type} | ${device.address ?? 'N/A'}' 
            ),
            trailing: device == _connectedDevice
              ? IconButton(
                  onPressed: _print,
                  icon: const Icon(Icons.print)
                )
              : null,
          );
        }
      ),
    );
  }

}
