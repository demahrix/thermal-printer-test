
import 'package:flutter/material.dart';
import 'package:test_print/menu_page.dart';

void main() {
  runApp(MaterialApp(
    title: 'Print test',
    theme: ThemeData(
      primarySwatch: Colors.blue,
    ),
    home: const MenuPage(),
  ));
}
