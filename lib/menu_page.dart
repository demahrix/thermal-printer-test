import 'package:flutter/material.dart';
import 'package:test_print/blue_thermal_printer_page.dart';
import 'package:test_print/esc_pos_bluetooth_page.dart';
import 'package:test_print/pinting_page.dart';

class MenuPage extends StatefulWidget {
  const MenuPage({ Key? key }) : super(key: key);

  @override
  State<MenuPage> createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {

  void _navigate(Widget Function(BuildContext) builder) {
    Navigator.of(context).push(MaterialPageRoute(builder: builder));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Menu'),
      ),
      body: ListView(
        children: [

          ListTile(
            title: const Text('esc_pos_bluetooth'),
            subtitle: const Text('Printer'),
            onTap: () => _navigate((_) => const EscPosBluetoothPage()),
          ),

          ListTile(
            title: const Text('blue_thermal_printer'),
            subtitle: const Text('Printer'),
            onTap: () => _navigate((_) => const BlueThermalPrinterPage()),
          ),

          ListTile(
            title: const Text('printing'),
            subtitle: const Text('Printing Service'),
            onTap: () => _navigate((_) => const PrintingPage()),
          ),

        ],
      ),
    );
  }
}
