import 'package:flutter/material.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';

class PrintingPage extends StatefulWidget {

  const PrintingPage({ Key? key }) : super(key: key);

  @override
  State<PrintingPage> createState() => _PrintingPageState();
}

class _PrintingPageState extends State<PrintingPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Printing page'),
      ),
      body: const SizedBox(),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {

          final doc = pw.Document();

          doc.addPage(pw.Page(
            pageFormat: PdfPageFormat.a4,
            build: (pw.Context context) {
              return pw.Center(
                child: pw.Text('Hello World'),
              ); // Center
            }));

          await Printing.layoutPdf(onLayout: (PdfPageFormat format) async => doc.save());
        },
        child: const Icon(Icons.print),
      ),
    );
  }
}
